.data
             n:       .asciiz "Enter value for n: "    #entrada de dados
             s:       .asciiz "Enter value for s: "    #entrada de dados
             RES:     .asciiz "Combination = "          #resutado da combinacao
             erromsg: .asciiz "Error code = "      #mensagem de erro
      
.text   
             li $v0,4       			      	       #indica q vai imprimir string
             la $a0,n       				           #imprime a string
             syscall        				           #chamada de sistema
              
             li $v0,5     	 			               #captura oq foi digitado
             syscall        				           #chamada de sistema
             move $t0,$v0   				           #$t0, possui o valor de n
              
              
             li $v0,4     				              #indica q vai imprimir string
             la $a0,s     				              #imprime a string
             syscall      				              #chamada de sistema
              
             li $v0,5      				              #captura oq foi digitado
             syscall          				          #chamada de sistema
             move $t1,$v0   				          #$t1 possui valor de s
              
              
             beq $t0,$zero,igualZero 			      #comparacao para codigo de erro 4
             beq $t1,$zero,igualZero
             
             blt $t0,$zero,negativo  			      #comparacao para codigo de erro 2 
             blt $t1,$zero,negativo
             
             beq $t0,$t1,nIguals  			          #comparacao para codigo de erro 2
             blt $t0,$t1,sMaiorn  			          #comparacao para codigo de erro 1
             
             sub $t2,$t0,$t1 				          #subtracao para obtencao de (n-s)!
             move $a0,$t0     				          #move valor de n, "$to", para $a0
             jal fat     				              #chama a funcao fat
             move $t0,$s3 				              #move valor q esta em $s3 para "$to", 
             move $a0,$zero  				          #libera $a0
          
             move $a0,$t1 				              #entrando no fatorial de s, $a0 tem o valor de s,$t1.
             jal fat    				              #chama funcao fat
             move $t1,$s3 				              #move o resultado para $t1
             move $a0,$zero				              #libera $a0
         
             move $a0,$t2  				              #entrando no fatorial de(n-s)!
             jal fat       				              #chama funcao fat
             move $t2,$s3 				              #mobe o resultado para $t2
             move $a0,$zero   				          #libera $a0
             j combina 					              #calculo da combinacao
  
  
fat:         move $s2,$a0     				          #move o valor de $a0 para $t2
             beq $s2,$zero,sai     				      #if($s2==0)
             sub $s2,$s2,1    				          #decrementa $s2
             or $s3,$zero,$a0    				      #$s3 tambem possui valor de $a0
            
for:         beq $s2,$zero,sai    			          #if($s2==0)
             mult $s3,$s2  			                  #multiplica $s3 por $s2
             sub $s2,$s2,1   			              #decrementa $s2
             mflo $s3  				                  #resultado da multiplicacao
             j for     				                  #jump para for
          
sai:         jr $ra  				                  #endereco de retorno
     
combina:     mult $t1,$t2   			              #multiplica os resultados do fatorial de $t1 por $t2
             mflo $t3      			                  #resultado da multiplicacao
                      
             div $t0,$t3    		                  # n! / ($t1*$t2)
             mflo $s4       			              # salva o resultado da multiplicacao
             move $a0,$zero    			              # libera $a0


RESULT:      li $v0,4 				                  #indica que vai imprimir string
             la $a0,RES 			                  #imprime a string
             syscall 				                  #chamada de sistema
                
             li $v0,1  				                  #indica que vai imprimir inteiro
             add $a0,$zero,$s4 			              #passa o resultado para $a0, e imprime
             syscall 				                  #chamada de sistema.
                
             j fim 				                      #jump fim 
              
igualZero:   ori $v1,4			                      #impressao dos codigos de erros
             j erro 				                  #jump para imprimir erro
     
negativo:    ori $v1,2 				                  #passa valor 2 para $v1
             j erro 				                  #jump para imprimir erro
     
     
nIguals:     ori $v1,3 				                  #passa valor 3 para $v1
             j erro 				                  #jump para imprimir erro
     
sMaiorn:     ori $v1,1 				                  #passa valor 1 para $v1
             j erro   				                  #jump para imprimir erro
     
erro:        li $v0,4 				                  #indica que vai imprimir string
             la $a0,erromsg  			              #imprime string
             syscall 				                  #chamada de sistema
     
             li $v0,1  				                  #indica que vai imprimir inteiro
             add $a0,$v1,0  			              #passa valor de $v1 para $a0, imprime inteiro
             syscall 				                  #chamada de sistema
      
fim:         nop
     
     
     
     
     
     
     
     