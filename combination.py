n = int(input("Enter value for n: "))
s = int(input("Enter value for s: "))

erro = -1
if n == 0 or s == 0:
    erro = 4
elif n < 0 or s < 0:
    erro = 2
elif n == s:
    erro = 3
elif s > n:
    erro = 1

def fat(num):
    RES = 1
    while num > 1:
        RES = RES * num
        num -= 1
    return RES

if erro < 0:
    fatorialN = fat(n)
    fatorialP = fat(s)
    fatorialNP = fat(n - s)
    RES =  float (fatorialN / (fatorialP * fatorialNP))
    print "Combination = ", RES
else:
    print "Error code = ", erro
