## The Assembly Machine

This is a machine implemented in Python aimed to simulate the process of translation of code in high level language to low level language, i.e. a machine language.

1. Dependencies
	* [Tabulate](https://pypi.python.org/pypi/tabulate)

2. Running

	* ``` python build_tables.py <assembly_file> ```
