# Created by Willame Soares
# 05-20-2016
# Building symbol tables and binary code from assembly

import fileinput
from tabulate import tabulate
from reference_tables import *
import sys

reload(sys)
sys.setdefaultencoding('utf8')

inst_itens = [] #keep instructions from the text section
data_itens = [] #keep instructions from the data section
symbol_table = {} #symbol + location
literal_table = {} #literal + location

def line_is_not_comment(line):
	return not line.startswith('#')

def line_is_not_blank(line):
	return bool(line.strip())

def is_number(value):
	if(value[:1] in '0123456789'):
		return True

def get_operands(line):
	itens = line.split()
	if (check_for_symbol(line)):
		return itens[2].split(',')
	else:
		return itens[1].split(',')

def check_for_symbol(line):
	itens = line.split()
	if (itens[0].endswith(':')):
		return itens[0]

def enter_new_symbol(symbol, location):
	global symbol_table
	symbol = symbol.rstrip(':')
	symbol_table[symbol] = location

def check_for_literal(line):
	literals = []
	operands = get_operands(line)	
	for i in range(len(operands)):
		if not(operands[i].startswith('$') or operands[i][:1] in '0123456789'):
			literals.append(operands[i])
	return literals

def enter_new_literal(literal,data_type,location,length):
	global literal_table
	literal_table[literal] = [data_type,location,length]

def extract_opcode(line):
	itens = line.split()
	if(check_for_symbol(line)):
		return itens[1]
	else:
		return itens[0]

def get_opcode_type(opcode, line_number):
	if (instructions_dic.has_key(opcode)):
		return instructions_dic[opcode][0]
	elif (pseudoinstructions_dic.has_key(opcode)):
		return 'P'
	else:
		print "Invalid opcode in line ", line_number+1
		return 'X' # illegal opcode

# Data section function
def get_data_type(line):
	aux = line.split()
	return aux[1]

# Data section function
def get_length_data(data_type, line):
	itens = line.split()
	if data_type == ".space":
		return itens[2]
	elif data_type == ".byte":
		values = itens[2].split(',')
		return len(values)
	else:
		return data_size[data_type]

# get length recursively
def get_length_inst(opcode_type, opcode, line_number):
	if (opcode_type == 'R'):
		return 0x4
	elif (opcode_type == 'I'):
		return 0x4
	elif (opcode_type == 'J'):
		return 0x4
	elif (opcode_type == 'P'):
		length = 0x0
		for i in range(len(pseudoinstructions_dic[opcode])):
			opcode_type = get_opcode_type(pseudoinstructions_dic[opcode][i], line_number)
			length += get_length_inst(opcode_type, opcode,line_number)
		return length

def write_file(itens, target):
	target.write(tabulate(itens, tablefmt="plain"))

def get_bin_code(opcode_type, opcode, length, line, line_number = 0):
	global literal_table
	operands = get_operands(line)
	if (opcode_type == 'R'):
		if (opcode == "syscall"):
			bin_string = "00000000000000000000000000" + \
						 instructions_dic[opcode][1] + "\n"
		elif (opcode == "nop"):
			bin_string = "00000000000000000000000000000000"
		else: 
			bin_string = "000000"
			if (len(operands) == 3):
				bin_string += register_codes[operands[1]]
				if (register_codes.has_key(operands[2])):
					bin_string += register_codes[operands[2]]
				else:
					bin_string += "00000"
				bin_string += register_codes[operands[0]] + \
							  "00000" + \
							  instructions_dic[opcode][1] + "\n"
			elif (len(operands) == 2):
				bin_string += register_codes[operands[0]] + \
							  register_codes[operands[1]] + \
							  "0000000000" + \
							  instructions_dic[opcode][1] + "\n"
			else:
				bin_string += register_codes[operands[0]] + \
						 	  "000000000000000" + \
						 	  instructions_dic[opcode][1] + "\n"
	elif (opcode_type == 'I'):
		bin_string = ''
		bin_string = instructions_dic[opcode][1]
		if (len(operands) == 3):
			bin_string += register_codes[operands[0]] + \
						  register_codes[operands[1]]
			if (symbol_table.has_key(operands[2])):
				bin_string += format(int(symbol_table[operands[2]]), '016b') + "\n"
			elif (type(operands[2]) is int):
				bin_string += format(int(operands[1]), '016b') + "\n"	  
		if (len(operands) == 2):
			if (register_codes.has_key(operands[1])):
				bin_string += register_codes[operands[1]] + \
							  register_codes[operand[0]] + \
							  format(int(literal_table[operands[1]][1]), '016b') + "\n"
			elif (literal_table.has_key(operands[1])):
				bin_string += "00000" + \
						 	  register_codes[operands[0]] + \
							  format(int(literal_table[operands[1]][1]), '016b') + "\n"
			elif (is_number(operands[1])):
				bin_string += "00000" + \
							  register_codes[operands[0]] + \
							  format(int(operands[1]), '016b') + "\n"
	elif (opcode_type == 'J'):
		bin_string = instructions_dic[opcode][1] + \
					 format(symbol_table[operands[0]], '026b')	+ "\n"
	elif (opcode_type == 'P'):
		bin_string = ''
		length = 0x0
		for i in range(len(pseudoinstructions_dic[opcode])):
			opcode_type = get_opcode_type(pseudoinstructions_dic[opcode][i], line_number)
			new_opcode = pseudoinstructions_dic[opcode][i]
			bin_string += get_bin_code(opcode_type, new_opcode, length, line, line_number)
	return bin_string

def main():
	global symbol_table,literal_table,inst_itens,data_itens
	
	# initialize location counter
	ILC = 0x100
	DLC = 0x1000
	# setup tem file
	tempfile = "temp.txt"
	target = open(tempfile, 'w')
	target.truncate() # erase file
	# signals for sections in the code
	data_section = False
	text_section = False

	# FIRST READING
	for count,line in enumerate(fileinput.FileInput(sys.argv[1:])):
		length_instruction = 0x0
		length_data = 0x0

		aux_itens = line.split()

		if (line_is_not_comment(line) and line_is_not_blank(line)):
			if aux_itens[0] == ".data":
				data_section = True
				continue
			if aux_itens[0] == ".text":
				text_section = True
				data_section = False
				continue

			if data_section: #DATA SECTION HANDLER
				if(check_for_symbol(line)):
					lit = check_for_symbol(line)
					lit = lit.rstrip(":")
					data_type = get_data_type(line)
					length_data = get_length_data(data_type, line)
					enter_new_literal(lit, data_type, DLC, length_data)
					data_itens.append([lit, data_type, hex(DLC), length_data])
					DLC += length_data
				else:
					print "invalid instruction"

			elif text_section: #TEXT SECTION HANDLER
				if line.startswith('.'): continue
				symbol = check_for_symbol(line)
				if(symbol != None):
					enter_new_symbol(symbol, ILC)

				opcode = extract_opcode(line)
				if opcode == 'nop':
					break
				op_type = get_opcode_type(opcode, count)
				
				if(op_type != 'X'):
					# get instruction length
					length_instruction = get_length_inst(op_type, opcode, count)

				inst_itens.append([op_type, opcode, length_instruction, line])
				# increment location counter
				ILC += length_instruction
	
	symbol_list = []
	literal_list = []
	for key,value in symbol_table.iteritems():
		temp = [key,hex(value)]
		symbol_list.append(temp)
	for key,value in literal_table.iteritems():
		temp = [key]
		for count,i in enumerate(value):
			if count == 1:
				temp.append(hex(i))
			else:
				temp.append(i)
		literal_list.append(temp)

	# Write file with tables generated
	table_file = open("tables_generated.txt", 'w')
	table_file.truncate()
	table_file.write(tabulate(symbol_list, 
		headers=("SYMBOL","ADDRESS"), tablefmt="fancy_grid") + "\n")
	table_file.write("\n" + tabulate(literal_list, 
		headers=("LITERAL","TYPE","ADDRESS","SIZE"), tablefmt="fancy_grid"))

	# BUILD TEMP FILE
	target.write(".data\n")
	write_file(data_itens, target)
	target.write("\n.text\n")
	write_file(inst_itens, target)		

	target.close()


	# SECOND READING
	data_section = False
	text_section = False
	itens = []
	bin_instructions = []
	ILC = 0x100

	finalfile = "combination_bin.txt"
	target = open(finalfile, 'w')
	target.truncate() # erase file

	for count,line in enumerate(fileinput.FileInput(tempfile)):
		if line == ".data":
			data_section = True
		elif line.startswith(".text"):
			data_section = False
			text_section = True

		if text_section:
			if line.startswith('.'): continue
			itens = line.split()
			op_type = itens[0]
			opcode = itens[1]
			length = itens[2]
			if(check_for_symbol(itens[3]) and opcode != "syscall"):
				line = ''.join(itens[4]) + ' ' + \
					   ''.join(itens[5]) + ' '
			else:
				line = ''.join(itens[3]) + ' ' + \
					   ''.join(itens[4])

			code = get_bin_code(op_type, opcode, length, line, count)
			bin_instructions.append(code)
	
	write_file(bin_instructions, target)

	target.close()

if __name__ == '__main__':
	main()